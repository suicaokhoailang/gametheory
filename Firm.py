from Engine import Engine
from Airline import Airline
import numpy as np


class Firm:
    def __init__(self):
        self.new_engines = []
        self.derivative_engines = []

        for thrust_level in range(1, 4):
            for e in range(0, 27):
                self.new_engines.append(Engine(e + (thrust_level - 1) * 27, thrust_level, 0))
                self.derivative_engines.append(Engine(e + (thrust_level - 1) * 27, thrust_level, 1))

        self.pcts = []
        self.pct_params = []
        # for new project
        mu, sigma = np.random.uniform(1.0, 1.6, 1)[0], 0.36  # mean and standard deviation
        self.pcts.append(np.random.lognormal(mu, sigma, 1000))
        self.pct_params.append([mu, sigma])
        # for derivative project
        mu, sigma = np.random.uniform(1.0, 1.3, 1)[0], 0.29  # mean and standard deviation
        self.pcts.append(np.random.lognormal(mu, sigma, 1000))
        self.pct_params.append([mu, sigma])

    def get_pcts(self):
        return self.pcts

    def get_pct_params(self):
        return self.pct_params

    def sort_new_engines(self, airlines):
        engine_tuples = []
        for engine in self.new_engines:
            scores = []
            for airline in airlines:
                score = np.sum(engine.data * airline.data)
                scores.append(score)
            engine_tuples.append((engine, np.sum(scores), scores))
        return sorted(engine_tuples, key=lambda e: e[1], reverse=True)

    def sort_derivative_engines(self, airlines):
        engine_tuples = []
        for engine in self.derivative_engines:
            scores = []
            for airline in airlines:
                score = np.sum(engine.data * airline.data)
                scores.append(score)
            engine_tuples.append((engine, np.sum(scores), scores))
        return sorted(engine_tuples, key=lambda e: e[1], reverse=True)

    def find_best_engines(self, market_A, market_B):
        best_engines = []
        best_engines.extend(self.sort_new_engines(market_A)[0:1])
        best_engines.extend(self.sort_new_engines(market_B)[0:1])
        best_engines.extend(self.sort_derivative_engines(market_A)[0:1])
        best_engines.extend(self.sort_derivative_engines(market_B)[0:1])
        return best_engines
