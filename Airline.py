import numpy as np


class Airline:
    def __init__(self, market):
        self.market = market
        self.performance_weight = np.random.uniform(0, 1, 1)
        self.economics_weight = np.random.uniform(0, 1, 1)
        self.maintenance_weight = np.random.uniform(0, 1, 1)
        self.far_weight = np.random.uniform(0, 1, 1)
        self.data = np.array([self.performance_weight, self.economics_weight, self.maintenance_weight, self.far_weight])

    def print_data(self):
        print self.data
