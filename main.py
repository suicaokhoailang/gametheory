import matplotlib
import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import sys
from Airline import Airline
from Firm import Firm
import gambit

if sys.version_info[0] < 3:
    import Tkinter as Tk
else:
    import tkinter as Tk

# Revenue and cost for pay off computing
R = np.array([10, 8])
X = np.array([3, 2])
# Fixed PCT for firm 1 and 2
T = np.array([[2.5, 3.5], [4, 2]])
discount_rate = 0.03

f1 = Firm()
f2 = Firm()
firms = [f1, f2]
market_A_airlines = []
market_B_airlines = []

for i in range(0, 10):
    market_A_airlines.append(Airline(0))
    market_B_airlines.append(Airline(1))

best_f1_engines = f1.find_best_engines(market_A_airlines, market_B_airlines)
best_f2_engines = f2.find_best_engines(market_A_airlines, market_B_airlines)

market_share = np.zeros((4, 4, 2))

index_i = 0
index_j = 0
for f1_engine in best_f1_engines:
    index_j = 0
    for f2_engine in best_f2_engines:
        for airline in range(0, 10):
            if f1_engine[2][airline] > f2_engine[2][airline]:
                market_share[index_i][index_j][0] += 10
            else:
                market_share[index_i][index_j][1] += 10
        index_j += 1
    index_i += 1


def compute_payoff(market_share):
    g = gambit.Game.new_table([4, 4])
    for r in range(0, 4):
        for c in range(0, 4):
            # new project
            project_type = 0
            if r >= 2:
                # derivative project
                project_type = 1

            t1 = np.average(f1.get_pcts()[project_type])
            t2 = np.average(f2.get_pcts()[project_type])
            r1 = R[project_type] * market_share[r][c][0] / 100
            r2 = R[project_type] * market_share[r][c][1] / 100
            # payoffs for each firm
            p1 = 0
            p2 = 0
            if t2 > t1:
                p1 = R[project_type] * (
                    np.exp(-discount_rate * t1) - np.exp(-discount_rate * t2)) / discount_rate + r1 * np.exp(
                    -discount_rate * t2) / discount_rate
                p2 = r2 * np.exp(
                    -discount_rate * t2) / discount_rate
            else:
                p2 = R[project_type] * (
                    np.exp(-discount_rate * t2) - np.exp(-discount_rate * t1)) / discount_rate + r2 * np.exp(
                    -discount_rate * t1) / discount_rate
                p1 = r1 * np.exp(
                    -discount_rate * t1) / discount_rate
            p1 -= X[project_type] * t1
            p2 -= X[project_type] * t2
            g[r, c][0] = int(p1)
            g[r, c][1] = int(p2)
    return g


# market_share = np.array([[[50, 50], [80, 20], [40, 60], [20, 80]],
#                          [[25, 75], [50, 50], [35, 65], [30, 70]],
#                          [[60, 40], [80, 20], [50, 50], [60, 40]],
#                          [[80, 20], [60, 40], [40, 60], [50, 50]]])

# payoff = np.array([[[146, 46], [185, 23], [26, 89], [89, 68]],
#                    [[11, 59], [154, 61], [6, 46], [12, 79]],
#                    [[240, 26], [240, 26], [29, 213], [36, 101]],
#                    [[159, 67], [285, 6], [6, 340], [36, 264]]])

game = compute_payoff(market_share)
for p in range(0, 2):
    game.players[p].label = "Firm " + str((p + 1))
    game.players[p].strategies[0].label = "New - Market A"
    game.players[p].strategies[1].label = "New - Market B"
    game.players[p].strategies[2].label = "Derivative - Market A"
    game.players[p].strategies[3].label = "Derivative - Market B"
# for row in range(0,4):
#     for col in range(0,4):
#         print game[row, col][0],game[row, col][1]
nashes = gambit.nash.enumpure_solve(game, use_strategic=True)
nash_cells = []
for i in range(0, len(nashes)):
    nash_cell = []
    idx = 0
    for fraction in list(nashes[i]):
        if fraction == 1:
            nash_cell.append(idx % 4)
        idx += 1
    nash_cells.append(nash_cell)
print "Nash Equilibrium positions by pure strategies: "
print nash_cells
solver = gambit.nash.ExternalEnumMixedSolver()
nashes = solver.solve(game)
print "Nash Equilibrium positions by mixed strategies: "
print nashes

#
# Drawing stuffs
#
font = {'weight': 'normal', 'size': 8}

matplotlib.rc('font', **font)
matplotlib.use('TkAgg')

pct_root = Tk.Tk()
pct_root.configure(background='#BFBFBF')
pct_root.wm_title("PCTs for Firms")

for i in range(0, 2):
    f = firms[i]

    for j in range(0, 2):
        fig = Figure(figsize=(3.42, 3), dpi=100, tight_layout=True)
        plt = fig.gca()
        plt.set_ylabel('Frequency')
        if j == 0:
            type_str = 'New PCT'
        else:
            type_str = 'Derivative PCT'
        plt.set_xlabel('Firm ' + str(i + 1) + ' ' + type_str + ', avg = ' + str(round(np.average(f.get_pcts()[j]), 3)))
        count, bins, ignored = plt.hist(f.get_pcts()[j], 100, normed=True, align='left')
        x = np.linspace(min(bins), max(bins), 10000)
        mu = f.get_pct_params()[j][0]
        sigma = f.get_pct_params()[j][1]

        pdf = (np.exp(-(np.log(x) - mu) ** 2 / (2 * sigma ** 2))
               / (x * sigma * np.sqrt(2 * np.pi)))
        plt.plot(x, pdf, linewidth=2, color='r')
        plt.axis('tight')
        # a tk.DrawingArea
        canvas = FigureCanvasTkAgg(fig, master=pct_root)
        # canvas.show()
        canvas.get_tk_widget().grid(row=i, column=j)

market_root = Tk.Tk()
market_root.configure(background='#BFBFBF')
market_root.wm_title("Market Share")

market_share_frame = Tk.Frame(market_root, width=500, height=400)
market_share_frame.grid(row=0, column=0)

payoff_root = Tk.Tk()
payoff_root.configure(background='#BFBFBF')
payoff_root.wm_title("Payoff")
payoff_frame = Tk.Frame(payoff_root, width=500, height=400)
payoff_frame.grid(row=0, column=1)

frames = [market_share_frame, payoff_frame]

for frame in frames:
    firm_name = Tk.Label(frame, text="Firm 2", font=('Source Code Pro', 20, "bold"))
    firm_name.grid(row=0, column=2, columnspan=4)

    firm_name = Tk.Label(frame, text="Firm 1", font=('Source Code Pro', 20, "bold"))
    firm_name.grid(row=2, column=0, rowspan=4)

    empty = Tk.Frame(frame, width=100, height=80, borderwidth=1, relief=Tk.FLAT)
    empty.grid(row=1, column=1)

    firm2_frame = Tk.Frame(frame, height=80, width=410, relief=Tk.RAISED)
    firm2_frame.grid(row=1, column=2, columnspan=4)

    n = list(nashes[0])

    for i in range(0, 2):
        f = Tk.Frame(firm2_frame, height=60, width=205)
        f.grid(row=1, column=2 * i + 1, columnspan=2)
        if i == 0:
            l = Tk.Label(f, text="N", font=('Source Code Pro', 15, "bold"), width=15, height=1, bg="red", fg="white",
                         bd=2)
            l.grid(row=0, column=0, columnspan=2)
            l.config(highlightbackground="red")

        else:
            l = Tk.Label(f, text="D", font=('Source Code Pro', 15, "bold"), width=15, height=1, bg="black",
                         fg="white", bd=2)
            l.config(highlightbackground="black")
            l.grid(row=0, column=0, columnspan=2)

        l = Tk.Label(f, text="A", font=('Source Code Pro', 14, "bold"), width=8, height=1, bg="#69b4ea", fg="white")
        l.grid(row=1, column=0)
        l = Tk.Label(f, text="B", font=('Source Code Pro', 14, "bold"), width=8, height=1, bg="#d17127", fg="white")
        l.grid(row=1, column=1)
        if frame == payoff_frame and len(nash_cells) == 0:
            l = Tk.Label(f, text=round(n[2 * i + 4], 3), font=('Source Code Pro', 14, "bold"), width=8, height=1,
                         bg="white",
                         fg="black")
            l.grid(row=2, column=0)
            l = Tk.Label(f, text=round(n[2 * i + 5], 3), font=('Source Code Pro', 14, "bold"), width=8, height=1,
                         bg="white",
                         fg="black")
            l.grid(row=2, column=1)

    firm1_frame = Tk.Frame(frame, width=100, borderwidth=1)
    firm1_frame.grid(row=2, column=1, rowspan=4)

    # i'm sorry, don't have time
    for i in range(0, 2):
        f = Tk.Frame(firm1_frame, width=140, height=100)
        f.grid(column=1, row=2 * i + 1, rowspan=2)
        if i == 0:
            l = Tk.Label(f, text="N", font=('Source Code Pro', 14, "bold"), width=2, height=4, bg="red", fg="white",
                         bd=2.5)
            l.grid(row=0, column=0, rowspan=2)
            l.config(highlightbackground="red")

        else:
            l = Tk.Label(f, text="D", font=('Source Code Pro', 14, "bold"), width=2, height=4, bg="black",
                         fg="white", bd=2.5)
            l.grid(row=0, column=0, rowspan=2)
            l.config(highlightbackground="black")

        l = Tk.Label(f, text="A", font=('Source Code Pro', 13, "bold"), width=1, height=2, bg="#69b4ea", fg="white",
                     bd=2.5)
        l.grid(row=0, column=1)
        l.config(highlightbackground="#69b4ea")

        l = Tk.Label(f, text="B", font=('Source Code Pro', 13, "bold"), width=1, height=2, bg="#d17127", fg="white",
                     bd=2.5)
        l.grid(row=1, column=1)
        l.config(highlightbackground="#d17127")
        if frame == payoff_frame and len(nash_cells) == 0:
            l = Tk.Label(f, text=round(n[2 * i], 3), font=('Source Code Pro', 14, "bold"), width=8, height=1,
                         bg="white",
                         fg="black")
            l.grid(row=0, column=2)
            l = Tk.Label(f, text=round(n[2 * i + 1], 3), font=('Source Code Pro', 14, "bold"), width=8, height=1,
                         bg="white",
                         fg="black")
            l.grid(row=1, column=2)

i = 0

for frame in frames:
    for row in range(2, 6):
        for col in range(2, 6):
            for cell in nash_cells:
                if cell[0] == row - 2 and cell[1] == col - 2:
                    bg = "red"
            m_frame = Tk.Frame(frame, width=125, height=100, relief=Tk.RAISED)
            if (row + col) % 2 == 0:
                bg = "#4fd15e"
            else:
                bg = "green"
            if i == 1:
                for cell in nash_cells:
                    if cell[0] == row - 2 and cell[1] == col - 2:
                        bg = "red"
                        break
            if i == 0:
                m_label = Tk.Label(m_frame,
                                   text="(" + str(int(market_share[row - 2][col - 2][0])) + " " + str(
                                       str(int(market_share[row - 2][col - 2][1]))) + ")",
                                   font=('Source Code Pro', 12, "bold"), width=9, height=2, bg=bg,
                                   fg="white")
            else:
                m_label = Tk.Label(m_frame, text="(" + str(game[row - 2, col - 2][0]) + " " + str(
                    str(game[row - 2, col - 2][1])) + ")",
                                   font=('Source Code Pro', 12, "bold"), width=9, height=2,
                                   bg=bg,
                                   fg="white")
            m_label.pack(fill=Tk.BOTH)
            m_frame.grid(row=row, column=col)
    i += 1

Tk.mainloop()
