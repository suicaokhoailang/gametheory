from Airline import Airline
from Firm import Firm
import numpy as np
import gambit

# Revenue and cost for pay off computing
R = np.array([10, 8])
X = np.array([6, 4])
# Fixed PCT for firm 1 and 2
T = np.array([[2.5, 3.5], [4, 2]])
discount_rate = 0.03

f1 = Firm()
f2 = Firm()
market_A_airlines = []
market_B_airlines = []

for i in range(0, 10):
    market_A_airlines.append(Airline(0))
    market_B_airlines.append(Airline(1))

best_f1_engines = f1.find_best_engines(market_A_airlines, market_B_airlines)
best_f2_engines = f2.find_best_engines(market_A_airlines, market_B_airlines)

market_share = np.zeros((4, 4, 2))

index_i = 0
index_j = 0
for f1_engine in best_f1_engines:
    index_j = 0
    for f2_engine in best_f2_engines:
        for airline in range(0, 10):
            if f1_engine[2][airline] > f2_engine[2][airline]:
                market_share[index_i][index_j][0] += 10
            else:
                market_share[index_i][index_j][1] += 10
        index_j += 1
    index_i += 1


def compute_payoff(market_share):
    g = gambit.Game.new_table([4, 4])
    for r in range(0, 4):
        for c in range(0, 4):
            # new project
            project_type = 0
            if r >= 2:
                # derivative project
                project_type = 1

            t1 = T[0, project_type]
            t2 = T[1, project_type]
            r1 = R[project_type] * market_share[r][c][0] / 100
            r2 = R[project_type] * market_share[r][c][1] / 100
            # payoffs for each firm
            p1 = 0
            p2 = 0
            if t2 > t1:
                p1 = R[project_type] * (
                    np.exp(-discount_rate * t1) - np.exp(-discount_rate * t2)) / discount_rate + r1 * np.exp(
                    -discount_rate * t2) / discount_rate
                p2 = r2 * np.exp(
                    -discount_rate * t2) / discount_rate
            else:
                p2 = R[project_type] * (
                    np.exp(-discount_rate * t2) - np.exp(-discount_rate * t1)) / discount_rate + r2 * np.exp(
                    -discount_rate * t1) / discount_rate
                p1 = r1 * np.exp(
                    -discount_rate * t1) / discount_rate
            p1 -= X[project_type] * t1
            p2 -= X[project_type] * t2
            g[r, c][0] = int(p1)
            g[r, c][1] = int(p2)
    return g


# market_share = np.array([[[50, 50], [80, 20], [40, 60], [20, 80]],
#                          [[25, 75], [50, 50], [35, 65], [30, 70]],
#                          [[60, 40], [80, 20], [50, 50], [60, 40]],
#                          [[80, 20], [60, 40], [40, 60], [50, 50]]])

# payoff = np.array([[[146, 46], [185, 23], [26, 89], [89, 68]],
#                    [[11, 59], [154, 61], [6, 46], [12, 79]],
#                    [[240, 26], [240, 26], [29, 213], [36, 101]],
#                    [[159, 67], [285, 6], [6, 340], [36, 264]]])

game = compute_payoff(market_share)
for p in range(0, 2):
    game.players[p].label = "Firm " + str((p + 1))
    game.players[p].strategies[0].label = "New - Market A"
    game.players[p].strategies[1].label = "New - Market B"
    game.players[p].strategies[2].label = "Derivative - Market A"
    game.players[p].strategies[3].label = "Derivative - Market B"
# for row in range(0,4):
#     for col in range(0,4):
#         print game[row, col][0],game[row, col][1]
nashes = gambit.nash.enumpure_solve(game, use_strategic=True)
nash_cells = []
for i in range(0, len(nashes)):
    nash_cell = []
    idx = 0
    for fraction in list(nashes[i]):
        if fraction == 1:
            nash_cell.append(idx % 4)
        idx += 1
    nash_cells.append(nash_cell)
print "Nash Equilibrium positions by pure strategies: "
print nash_cells
solver = gambit.nash.ExternalEnumMixedSolver()
nashes = solver.solve(game)
print "Nash Equilibrium positions by mixed strategies: "
print nashes
