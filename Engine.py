import numpy as np


class Engine:
    # 0 = new, 1 = derivative
    def __init__(self, id, thrust_level, engine_type=0):
        # TODO random
        self.id = id
        self.type = engine_type
        self.thrust_level = thrust_level
        self.performance = np.random.uniform(2 + thrust_level, 10, 1)
        self.economics = np.random.uniform(2, 10 - thrust_level, 1)
        self.maintenance = np.random.uniform(2, 10 - thrust_level, 1)
        self.far = np.random.uniform(5, 10, 1)
        self.data = np.array([self.performance, self.economics, self.maintenance, self.far])

    def print_data(self):
        print self.id, self.data

    def attraction_value(self, airline):
        return np.sum(self.data * airline.data)

    def total_attraction_value(self, airlines):
        total = 0
        for airline in airlines:
            total += self.attraction_value(airline)
        return total

